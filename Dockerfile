# Dockerfile to build and run the module on the cloud
# Stage 1 s3 push
FROM d3fk/s3cmd AS s3-bucket
RUN mkdir /home/payload
WORKDIR /home/payload
ARG S3_AKEY
ARG S3_SKEY
ARG NODAL_NAME
ARG S3_LAB_MANUAL
ARG S3_HOST
RUN ls -ltr 
RUN s3cmd get --recursive $S3_LAB_MANUAL/$NODAL_NAME/ --host-bucket=bucket --host=$S3_HOST --no-check-certificate --access_key=$S3_AKEY --secret_key=$S3_SKEY
# RUN ls -ltr payload

# Stage 2 httpd serving
FROM alpine:latest AS webhost
WORKDIR /var/www/localhost/htdocs
RUN apk update \
    && apk add lighttpd \
    && rm -rf /var/cache/apk/*
COPY --from=s3-bucket /home/payload /var/www/localhost/htdocs/
RUN ls -ltr /var/www/localhost/htdocs/
EXPOSE 80
CMD ["lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf"]
